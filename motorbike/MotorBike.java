package motorbike;

public class MotorBike {
    private int speed;                      //member Variable
    void start(){
        System.out.println("Bike started");
    }
    MotorBike(int speed){
        this.speed = speed;
    }
    void setSpeed(int speed){               //local variable
        if(speed>0)
            this.speed = speed;
//        System.out.println(speed);        // local variable speed
//        System.out.println(this.speed);   //member variable speed
    }

    void increaseSpeed(int amount){
        this.speed += amount;
    }

    void decreaseSpeed(int amount){
        this.speed -= amount;
    }
    public int getSpeed() {
        return speed;
    }
}