package motorbike;

public class MotorBikeRunner {
    public static void main(String[] args) {
        MotorBike honda = new MotorBike(100);
        MotorBike suzuki = new MotorBike(150);

        honda.start();
        suzuki.start();
        honda.setSpeed(45);
        honda.increaseSpeed(100);

        suzuki.increaseSpeed(10);
        System.out.println("Honda is running at: "+honda.getSpeed());

//        honda.speed = 50;     //After member Variable is changed to private, can't access through object
//        suzuki.speed = 100;



    }
}
