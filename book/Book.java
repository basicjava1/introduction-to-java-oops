package book;

public class Book {
    private int noOfCopies;
    void reading(){
        System.out.println("Reading Book");
    }
    void completed(){
        System.out.println("Completed this book");
    }
    Book(int noOfCopies){
        this.noOfCopies = noOfCopies;
    }

    public void setNoOfCopies(int noOfCopies) {
        this.noOfCopies = noOfCopies;
    }
}
