package book;

public class BookRunner {
    public static void main(String[] args) {
        Book artOfComputerProgramming = new Book(1250);
        Book effectiveJava = new Book(1500);
        Book cleanCode = new Book(1750);

        artOfComputerProgramming.completed();
        effectiveJava.reading();
        cleanCode.reading();

        artOfComputerProgramming.setNoOfCopies(1000);
        effectiveJava.setNoOfCopies(222);
        cleanCode.setNoOfCopies(43543);
    }
}
